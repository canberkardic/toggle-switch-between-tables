var t1 = document.getElementById("toggle1"),
  t2 = document.getElementById("toggle2"),
  switcher = document.getElementById("switcher"),
  o1 = document.getElementById("option1"),
  o2 = document.getElementById("option2");

t1.addEventListener("click", function () {
  switcher.checked = false;
  t1.classList.add("toggler--is-active");
  t2.classList.remove("toggler--is-active");
  o1.classList.remove("hide");
  o2.classList.add("hide");
});

t2.addEventListener("click", function () {
  switcher.checked = true;
  t2.classList.add("toggler--is-active");
  t1.classList.remove("toggler--is-active");
  o1.classList.add("hide");
  o2.classList.remove("hide");
});

switcher.addEventListener("click", function () {
  t2.classList.toggle("toggler--is-active");
  t1.classList.toggle("toggler--is-active");
  o1.classList.toggle("hide");
  o2.classList.toggle("hide");
})