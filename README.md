## Toggle Switch Between Tables

----
- **Introduction**

This project aims to use the area of tables efficiently when developing SPA.

- **Example Usage**

For example, if you are developing a user interface that will contain 
2 tables relational to each other as basic version and advanced version of data 
visualization, it's not logical to use buttons for each table. With the help of
2 sides of toggle switch, you can show 2 table in 1 division and switch between 
them in just 1 click.

- **Including Libraries**

Project has developed with Bootstrap 4.

